Jott Link Drupal module

** DESCRIPTION **
This module acts as a Jott Link for the Jott.com transcription service. Details of the Jott service can be found at http://jott.com but the gist of it 
is that with this module and a Jott account you can effectively post to your Drupal website from a phone. Jott transcribes your voice into text, 
sends it to your website, and this module creates a post for it.

** INSTALLATION **
Enable the module under Administer >> Site building >> Modules.

The module creates a new content type known as a jott.

Set permissions for creating and editing one's own jott at Administer >> User management >> Access control.

Create an account (it's free) at http://Jott.com.

On Jott.com add a 'Jott Link' with the following parameters:

Link Name: <whatever you want to call this link - perhaps Drupal, or the name of your website>
Setup Url: http://www.example.com/jottsetup (where example is your website where this module is installed)
Link Url: http://www.example.com/jottlink
(Note: you must reference jottsetup and jottlink, they are pages used by the module)

When you click save you will be redirected to your website which saves your Jott userKey which allows you to make this a Jott Link. You must already be logged in to you site when you submit the Jott Link or your Jott Link will not be active.

** USAGE **

Set permissions for Jott at Admin >> User management >> Access control and setup the jott content type at Admin >> Content management >> Content types.

Under Admin >> Site configuration >> Jott you can configure Jott to post messages with low confidence and setup parsing.

Call Jott from the phone number you set your Jott.com account up with and say the name of the Jott Link you configured above and then your message.

If you have parsing enabled (on by default) if you say the separator (see Jott configuration) your message will be split into the posts title and body. 

Jott.com will send you an email or SMS when your jott is transcribed, at which point it will have been posted to your website.

** SUPPORT **
Please use the issue queue at http://drupal.org/project/jott.

** CREDITS **
Project maintainer: Ben Jeavons (Drupal.org username: coltrane) (jeavons AT gmail DOT com) but please use the issue queue for support!

Jott.com and the Jott service have their own license and Terms of Service. It is your responsibily to read and adhere to those.

